const { router, Joi } = require('suretrack-backend');
const SnippetQL       = require('snippetql');


const userDocs    = SnippetQL.collection("users");
const usersRouter = router();

usersRouter.post("/signup", (req, res) => {

    const { error } = Joi.object({
        username: Joi.string().required().messages({
            "string.base": "Parameter username wajib string",
            "any.required": "Parameter username wajib diisi"
        }),
        password: Joi.string().required().messages({
            "string.base": "Parameter password wajib string",
            "any.required": "Parameter password wajib diisi"
        }),
    }).validate(req.body);

    if(error){
        return res.status(500).send({
            message: error.details[0].message,
            responseCode: 500
        })
    }

    const checkUserExist = userDocs.select('*').where("username").equal(req.body.username).build();
    console.log(checkUserExist);
    
    if(checkUserExist[0].username == req.body.username){
        return res.status(403).send({
            message: 'Username telah digunakan.',
            responseCode: 403
        });
    } else {
        const insert = userDocs.insert({ username: req.body.username, password: req.body.password});
        if(insert){
            return res.status(200).send({
                message: 'berhasil signup',
                responseCode: 200
            });
        }
    }

});

module.exports = usersRouter;