const { app, json, cors } = require('suretrack-backend');
const usersRouter         = require('./users');
const SnippetQL           = require('snippetql');


SnippetQL.initializeDB();

app.use(json());
app.use(cors());

app.use('/users', usersRouter);
